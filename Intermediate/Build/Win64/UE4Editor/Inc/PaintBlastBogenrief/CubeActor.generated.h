// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBLASTBOGENRIEF_CubeActor_generated_h
#error "CubeActor.generated.h already included, missing '#pragma once' in CubeActor.h"
#endif
#define PAINTBLASTBOGENRIEF_CubeActor_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_RPC_WRAPPERS
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACubeActor(); \
	friend struct Z_Construct_UClass_ACubeActor_Statics; \
public: \
	DECLARE_CLASS(ACubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesACubeActor(); \
	friend struct Z_Construct_UClass_ACubeActor_Statics; \
public: \
	DECLARE_CLASS(ACubeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(ACubeActor)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor(ACubeActor&&); \
	NO_API ACubeActor(const ACubeActor&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActor(ACubeActor&&); \
	NO_API ACubeActor(const ACubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACubeActor)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ACubeActor, mesh); }


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_10_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class ACubeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_CubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
