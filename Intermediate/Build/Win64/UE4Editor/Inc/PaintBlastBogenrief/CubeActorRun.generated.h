// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBLASTBOGENRIEF_CubeActorRun_generated_h
#error "CubeActorRun.generated.h already included, missing '#pragma once' in CubeActorRun.h"
#endif
#define PAINTBLASTBOGENRIEF_CubeActorRun_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_RPC_WRAPPERS
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACubeActorRun(); \
	friend struct Z_Construct_UClass_ACubeActorRun_Statics; \
public: \
	DECLARE_CLASS(ACubeActorRun, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(ACubeActorRun)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_INCLASS \
private: \
	static void StaticRegisterNativesACubeActorRun(); \
	friend struct Z_Construct_UClass_ACubeActorRun_Statics; \
public: \
	DECLARE_CLASS(ACubeActorRun, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(ACubeActorRun)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACubeActorRun(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACubeActorRun) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActorRun); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActorRun); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActorRun(ACubeActorRun&&); \
	NO_API ACubeActorRun(const ACubeActorRun&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeActorRun(ACubeActorRun&&); \
	NO_API ACubeActorRun(const ACubeActorRun&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeActorRun); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeActorRun); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACubeActorRun)


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ACubeActorRun, mesh); }


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_8_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class ACubeActorRun>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_CubeActorRun_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
