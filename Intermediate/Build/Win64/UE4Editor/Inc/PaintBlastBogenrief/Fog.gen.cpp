// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintBlastBogenrief/Fog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFog() {}
// Cross Module References
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_AFog_NoRegister();
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_AFog();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PaintBlastBogenrief();
	PAINTBLASTBOGENRIEF_API UFunction* Z_Construct_UFunction_AFog_getSize();
	PAINTBLASTBOGENRIEF_API UFunction* Z_Construct_UFunction_AFog_revealSmoothCircle();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	PAINTBLASTBOGENRIEF_API UFunction* Z_Construct_UFunction_AFog_setSize();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AFog::StaticRegisterNativesAFog()
	{
		UClass* Class = AFog::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "getSize", &AFog::execgetSize },
			{ "revealSmoothCircle", &AFog::execrevealSmoothCircle },
			{ "setSize", &AFog::execsetSize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AFog_getSize_Statics
	{
		struct Fog_eventgetSize_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFog_getSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Fog_eventgetSize_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFog_getSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFog_getSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFog_getSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game" },
		{ "Comment", "// Set the plane's size\n" },
		{ "ModuleRelativePath", "Fog.h" },
		{ "ToolTip", "Set the plane's size" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFog_getSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFog, nullptr, "getSize", nullptr, nullptr, sizeof(Fog_eventgetSize_Parms), Z_Construct_UFunction_AFog_getSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_getSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFog_getSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_getSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFog_getSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFog_getSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFog_revealSmoothCircle_Statics
	{
		struct Fog_eventrevealSmoothCircle_Parms
		{
			FVector2D pos;
			float radius;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pos_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_pos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_radius = { "radius", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Fog_eventrevealSmoothCircle_Parms, radius), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_pos_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_pos = { "pos", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Fog_eventrevealSmoothCircle_Parms, pos), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_pos_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_pos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::NewProp_pos,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game" },
		{ "Comment", "// Reveal (make transparent) a portion of the plane\n" },
		{ "ModuleRelativePath", "Fog.h" },
		{ "ToolTip", "Reveal (make transparent) a portion of the plane" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFog, nullptr, "revealSmoothCircle", nullptr, nullptr, sizeof(Fog_eventrevealSmoothCircle_Parms), Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFog_revealSmoothCircle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFog_revealSmoothCircle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AFog_setSize_Statics
	{
		struct Fog_eventsetSize_Parms
		{
			float s;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_s;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AFog_setSize_Statics::NewProp_s = { "s", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Fog_eventsetSize_Parms, s), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AFog_setSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AFog_setSize_Statics::NewProp_s,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AFog_setSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Game" },
		{ "Comment", "// Set the plane's size\n" },
		{ "ModuleRelativePath", "Fog.h" },
		{ "ToolTip", "Set the plane's size" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AFog_setSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AFog, nullptr, "setSize", nullptr, nullptr, sizeof(Fog_eventsetSize_Parms), Z_Construct_UFunction_AFog_setSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_setSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AFog_setSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AFog_setSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AFog_setSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AFog_setSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AFog_NoRegister()
	{
		return AFog::StaticClass();
	}
	struct Z_Construct_UClass_AFog_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_dynamicTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_dynamicTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_m_squarePlane_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_m_squarePlane;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFog_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintBlastBogenrief,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AFog_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AFog_getSize, "getSize" }, // 3366234733
		{ &Z_Construct_UFunction_AFog_revealSmoothCircle, "revealSmoothCircle" }, // 2642601781
		{ &Z_Construct_UFunction_AFog_setSize, "setSize" }, // 4004628448
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Fog.h" },
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance = { "m_dynamicMaterialInstance", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial = { "m_dynamicMaterial", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData[] = {
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture = { "m_dynamicTexture", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_dynamicTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Fog.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane = { "m_squarePlane", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFog, m_squarePlane), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFog_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_dynamicTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFog_Statics::NewProp_m_squarePlane,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFog_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFog>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFog_Statics::ClassParams = {
		&AFog::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AFog_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFog_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFog_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFog()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFog_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFog, 3977681662);
	template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<AFog>()
	{
		return AFog::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFog(Z_Construct_UClass_AFog, &AFog::StaticClass, TEXT("/Script/PaintBlastBogenrief"), TEXT("AFog"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFog);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
