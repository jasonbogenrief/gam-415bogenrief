// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector2D;
#ifdef PAINTBLASTBOGENRIEF_Fog_generated_h
#error "Fog.generated.h already included, missing '#pragma once' in Fog.h"
#endif
#define PAINTBLASTBOGENRIEF_Fog_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execrevealSmoothCircle) \
	{ \
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_pos); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_radius); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->revealSmoothCircle(Z_Param_Out_pos,Z_Param_radius); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetSize) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->getSize(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetSize) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_s); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->setSize(Z_Param_s); \
		P_NATIVE_END; \
	}


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend struct Z_Construct_UClass_AFog_Statics; \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(AFog)


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAFog(); \
	friend struct Z_Construct_UClass_AFog_Statics; \
public: \
	DECLARE_CLASS(AFog, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(AFog)


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFog(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFog) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFog(AFog&&); \
	NO_API AFog(const AFog&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFog); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFog); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFog)


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__m_squarePlane() { return STRUCT_OFFSET(AFog, m_squarePlane); } \
	FORCEINLINE static uint32 __PPO__m_dynamicTexture() { return STRUCT_OFFSET(AFog, m_dynamicTexture); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterial() { return STRUCT_OFFSET(AFog, m_dynamicMaterial); } \
	FORCEINLINE static uint32 __PPO__m_dynamicMaterialInstance() { return STRUCT_OFFSET(AFog, m_dynamicMaterialInstance); }


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_10_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class AFog>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_Fog_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
