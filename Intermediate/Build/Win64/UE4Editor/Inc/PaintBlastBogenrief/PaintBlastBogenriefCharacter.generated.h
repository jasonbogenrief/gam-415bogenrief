// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBLASTBOGENRIEF_PaintBlastBogenriefCharacter_generated_h
#error "PaintBlastBogenriefCharacter.generated.h already included, missing '#pragma once' in PaintBlastBogenriefCharacter.h"
#endif
#define PAINTBLASTBOGENRIEF_PaintBlastBogenriefCharacter_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_RPC_WRAPPERS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefCharacter(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefCharacter_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefCharacter)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefCharacter(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefCharacter_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefCharacter)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBlastBogenriefCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBlastBogenriefCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefCharacter(APaintBlastBogenriefCharacter&&); \
	NO_API APaintBlastBogenriefCharacter(const APaintBlastBogenriefCharacter&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefCharacter(APaintBlastBogenriefCharacter&&); \
	NO_API APaintBlastBogenriefCharacter(const APaintBlastBogenriefCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBlastBogenriefCharacter)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(APaintBlastBogenriefCharacter, L_MotionController); }


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_11_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class APaintBlastBogenriefCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
