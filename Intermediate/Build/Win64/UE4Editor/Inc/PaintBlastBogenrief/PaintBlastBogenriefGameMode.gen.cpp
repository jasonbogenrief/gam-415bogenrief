// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintBlastBogenrief/PaintBlastBogenriefGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintBlastBogenriefGameMode() {}
// Cross Module References
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_APaintBlastBogenriefGameMode_NoRegister();
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_APaintBlastBogenriefGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PaintBlastBogenrief();
// End Cross Module References
	void APaintBlastBogenriefGameMode::StaticRegisterNativesAPaintBlastBogenriefGameMode()
	{
	}
	UClass* Z_Construct_UClass_APaintBlastBogenriefGameMode_NoRegister()
	{
		return APaintBlastBogenriefGameMode::StaticClass();
	}
	struct Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintBlastBogenrief,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PaintBlastBogenriefGameMode.h" },
		{ "ModuleRelativePath", "PaintBlastBogenriefGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaintBlastBogenriefGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::ClassParams = {
		&APaintBlastBogenriefGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaintBlastBogenriefGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintBlastBogenriefGameMode, 89192056);
	template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<APaintBlastBogenriefGameMode>()
	{
		return APaintBlastBogenriefGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintBlastBogenriefGameMode(Z_Construct_UClass_APaintBlastBogenriefGameMode, &APaintBlastBogenriefGameMode::StaticClass, TEXT("/Script/PaintBlastBogenrief"), TEXT("APaintBlastBogenriefGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintBlastBogenriefGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
