// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBLASTBOGENRIEF_PaintBlastBogenriefGameMode_generated_h
#error "PaintBlastBogenriefGameMode.generated.h already included, missing '#pragma once' in PaintBlastBogenriefGameMode.h"
#endif
#define PAINTBLASTBOGENRIEF_PaintBlastBogenriefGameMode_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_RPC_WRAPPERS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefGameMode(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), PAINTBLASTBOGENRIEF_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefGameMode)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefGameMode(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefGameMode_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), PAINTBLASTBOGENRIEF_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefGameMode)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PAINTBLASTBOGENRIEF_API APaintBlastBogenriefGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBlastBogenriefGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBLASTBOGENRIEF_API, APaintBlastBogenriefGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBLASTBOGENRIEF_API APaintBlastBogenriefGameMode(APaintBlastBogenriefGameMode&&); \
	PAINTBLASTBOGENRIEF_API APaintBlastBogenriefGameMode(const APaintBlastBogenriefGameMode&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PAINTBLASTBOGENRIEF_API APaintBlastBogenriefGameMode(APaintBlastBogenriefGameMode&&); \
	PAINTBLASTBOGENRIEF_API APaintBlastBogenriefGameMode(const APaintBlastBogenriefGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PAINTBLASTBOGENRIEF_API, APaintBlastBogenriefGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBlastBogenriefGameMode)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_9_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class APaintBlastBogenriefGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
