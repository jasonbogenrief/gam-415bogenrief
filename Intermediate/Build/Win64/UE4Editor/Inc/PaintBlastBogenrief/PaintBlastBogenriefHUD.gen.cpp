// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaintBlastBogenrief/PaintBlastBogenriefHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaintBlastBogenriefHUD() {}
// Cross Module References
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_APaintBlastBogenriefHUD_NoRegister();
	PAINTBLASTBOGENRIEF_API UClass* Z_Construct_UClass_APaintBlastBogenriefHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_PaintBlastBogenrief();
// End Cross Module References
	void APaintBlastBogenriefHUD::StaticRegisterNativesAPaintBlastBogenriefHUD()
	{
	}
	UClass* Z_Construct_UClass_APaintBlastBogenriefHUD_NoRegister()
	{
		return APaintBlastBogenriefHUD::StaticClass();
	}
	struct Z_Construct_UClass_APaintBlastBogenriefHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_PaintBlastBogenrief,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "PaintBlastBogenriefHUD.h" },
		{ "ModuleRelativePath", "PaintBlastBogenriefHUD.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APaintBlastBogenriefHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::ClassParams = {
		&APaintBlastBogenriefHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APaintBlastBogenriefHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APaintBlastBogenriefHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APaintBlastBogenriefHUD, 244749733);
	template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<APaintBlastBogenriefHUD>()
	{
		return APaintBlastBogenriefHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APaintBlastBogenriefHUD(Z_Construct_UClass_APaintBlastBogenriefHUD, &APaintBlastBogenriefHUD::StaticClass, TEXT("/Script/PaintBlastBogenrief"), TEXT("APaintBlastBogenriefHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APaintBlastBogenriefHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
