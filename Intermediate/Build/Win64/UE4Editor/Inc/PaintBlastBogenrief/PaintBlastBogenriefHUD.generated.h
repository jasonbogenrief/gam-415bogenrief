// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAINTBLASTBOGENRIEF_PaintBlastBogenriefHUD_generated_h
#error "PaintBlastBogenriefHUD.generated.h already included, missing '#pragma once' in PaintBlastBogenriefHUD.h"
#endif
#define PAINTBLASTBOGENRIEF_PaintBlastBogenriefHUD_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_RPC_WRAPPERS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefHUD(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefHUD_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefHUD)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefHUD(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefHUD_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefHUD)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBlastBogenriefHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBlastBogenriefHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefHUD(APaintBlastBogenriefHUD&&); \
	NO_API APaintBlastBogenriefHUD(const APaintBlastBogenriefHUD&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefHUD(APaintBlastBogenriefHUD&&); \
	NO_API APaintBlastBogenriefHUD(const APaintBlastBogenriefHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBlastBogenriefHUD)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_9_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class APaintBlastBogenriefHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
