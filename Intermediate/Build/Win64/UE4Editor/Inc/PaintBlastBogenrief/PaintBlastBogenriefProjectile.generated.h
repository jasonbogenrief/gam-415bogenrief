// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef PAINTBLASTBOGENRIEF_PaintBlastBogenriefProjectile_generated_h
#error "PaintBlastBogenriefProjectile.generated.h already included, missing '#pragma once' in PaintBlastBogenriefProjectile.h"
#endif
#define PAINTBLASTBOGENRIEF_PaintBlastBogenriefProjectile_generated_h

#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_SPARSE_DATA
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefProjectile(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefProjectile_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPaintBlastBogenriefProjectile(); \
	friend struct Z_Construct_UClass_APaintBlastBogenriefProjectile_Statics; \
public: \
	DECLARE_CLASS(APaintBlastBogenriefProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PaintBlastBogenrief"), NO_API) \
	DECLARE_SERIALIZER(APaintBlastBogenriefProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APaintBlastBogenriefProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APaintBlastBogenriefProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefProjectile(APaintBlastBogenriefProjectile&&); \
	NO_API APaintBlastBogenriefProjectile(const APaintBlastBogenriefProjectile&); \
public:


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APaintBlastBogenriefProjectile(APaintBlastBogenriefProjectile&&); \
	NO_API APaintBlastBogenriefProjectile(const APaintBlastBogenriefProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APaintBlastBogenriefProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APaintBlastBogenriefProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APaintBlastBogenriefProjectile)


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(APaintBlastBogenriefProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(APaintBlastBogenriefProjectile, ProjectileMovement); }


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_9_PROLOG
#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_RPC_WRAPPERS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_INCLASS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_SPARSE_DATA \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_INCLASS_NO_PURE_DECLS \
	gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAINTBLASTBOGENRIEF_API UClass* StaticClass<class APaintBlastBogenriefProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gam_415bogenrief_Source_PaintBlastBogenrief_PaintBlastBogenriefProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
