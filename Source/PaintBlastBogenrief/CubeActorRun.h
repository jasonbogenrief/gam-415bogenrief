#pragma once

#include "Components/RuntimeMeshComponentStatic.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CubeActorRun.generated.h"

UCLASS()
class PAINTBLASTBOGENRIEF_API ACubeActorRun : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACubeActorRun();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	//Defines all functions 
	virtual void GenerateBoxMesh();
	virtual void CreateBoxMesh(FVector BoxRadius, TArray < FVector >& Vertices, TArray < int32 >& Triangles, TArray < FVector >& Normals, TArray < FVector2D >& UVs, TArray < FRuntimeMeshTangent >& Tangents, TArray < FColor >& Colors);

private:
	UPROPERTY(VisibleAnywhere) URuntimeMeshComponentStatic* mesh;


};
