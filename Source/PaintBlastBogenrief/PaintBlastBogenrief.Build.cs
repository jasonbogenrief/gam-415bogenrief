// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;



public class PaintBlastBogenrief : ModuleRules
{
	public PaintBlastBogenrief(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ProceduralMeshComponent", "RuntimeMeshComponent" });

	}
}
